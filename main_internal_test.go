package main

import (
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestLevelToStrDebug(t *testing.T) {
	level := logrus.DebugLevel
	str := levelToStr(level)
	assert.Equal(t, str, "debug", "%v must corresponding to debug string", level)
}

func TestLevelToStrInfo(t *testing.T) {
	level := logrus.InfoLevel
	str := levelToStr(level)
	assert.Equal(t, str, "info", "%v must corresponding to info string", level)
}

func TestLevelToStrWarn(t *testing.T) {
	level := logrus.WarnLevel
	str := levelToStr(level)
	assert.Equal(t, "warning", str, "%v must corresponding to warning string", level)
}

func TestLevelToStrError(t *testing.T) {
	level := logrus.ErrorLevel
	str := levelToStr(level)
	assert.Equal(t, "error", str, "%v must corresponding to error string", level)
}

func TestLevelToStrFatal(t *testing.T) {
	level := logrus.FatalLevel
	str := levelToStr(level)
	assert.Equal(t, "fatal", str, "%v must corresponding to fatal string", level)
}

func TestLevelToStrPanic(t *testing.T) {
	level := logrus.PanicLevel
	str := levelToStr(level)
	assert.Equal(t, "panic", str, "%v must corresponding to panic string", level)
}

func TestLevelToStrUnknown(t *testing.T) {
	level := logrus.DebugLevel + logrus.InfoLevel + logrus.WarnLevel + logrus.ErrorLevel + logrus.FatalLevel + logrus.PanicLevel
	str := levelToStr(level)
	assert.Equal(t, "unknown", str, "%v must corresponding to unknown string", level)
}

func createRandomDir(t *testing.T, rootDir string) string {
	dir, err := ioutil.TempDir(rootDir, "")
	assert.Nil(t, err)
	return dir
}

func removeDirAll(t *testing.T, dir string) {
	assert.Nil(t, os.RemoveAll(dir))
}

func touchRandomFile(t *testing.T, dir string) string {
	f, err := ioutil.TempFile(dir, "")
	defer f.Close()
	assert.Nil(t, err)
	return f.Name()
}

func touchRandomFiles(t *testing.T, dir string, count int) []string {
	names := make([]string, count)

	for i := 0; i < count; i++ {
		names[i] = touchRandomFile(t, dir)
	}

	return names
}

func TestWalkDirEmpty(t *testing.T) {
	dir := createRandomDir(t, "")
	val := 0

	walkDir(dir, "", "", ioutil.ReadDir, func(srcPath string, dstPath string, f os.FileInfo) {
		val++
	})

	assert.Equal(t, 0, val)
	removeDirAll(t, dir)
}

func TestWalkDirSimple(t *testing.T) {
	const count = 10

	dir := createRandomDir(t, "")
	touchRandomFiles(t, dir, count)
	val := 0

	walkDir(dir, "", "", ioutil.ReadDir, func(srcPath string, dstPath string, f os.FileInfo) {
		val++
	})

	assert.Equal(t, count, val)
	removeDirAll(t, dir)
}

func TestWalkDirComplex(t *testing.T) {
	const count = 5

	// 8 dirs, each dir contribute to increment except root
	// therefore expected incremented value should be 8 * count + 8 - 1
	rootDir := createRandomDir(t, "")
	touchRandomFiles(t, rootDir, count)

	dir1 := createRandomDir(t, rootDir)
	touchRandomFiles(t, dir1, count)

	dir11 := createRandomDir(t, dir1)
	touchRandomFiles(t, dir11, count)

	dir2 := createRandomDir(t, rootDir)
	touchRandomFiles(t, dir2, count)

	dir21 := createRandomDir(t, dir2)
	touchRandomFiles(t, dir21, count)

	dir211 := createRandomDir(t, dir21)
	touchRandomFiles(t, dir211, count)

	dir212 := createRandomDir(t, dir21)
	touchRandomFiles(t, dir212, count)

	dir3 := createRandomDir(t, rootDir)
	touchRandomFiles(t, dir3, count)

	val := 0

	walkDir(rootDir, "", "", ioutil.ReadDir, func(srcPath string, dstPath string, f os.FileInfo) {
		val++
	})

	assert.Equal(t, 8*count+8-1, val)
	removeDirAll(t, rootDir)
}

func TestIsSimilarDataBothEmpty(t *testing.T) {
	emptyBytes1 := make([]byte, 0)
	emptyBytes2 := make([]byte, 0)
	assert.True(t, isSimilarData(emptyBytes1, emptyBytes2))
}

func TestIsSimilarDataSome(t *testing.T) {
	const str = "Hello World"
	someBytes1 := []byte(str)
	someBytes2 := []byte(str)
	assert.True(t, isSimilarData(someBytes1, someBytes2))
}

func TestIsSimilarDataDiff(t *testing.T) {
	const str1 = "Hello1"
	someBytes1 := []byte(str1)

	const str2 = "Hello2"
	someBytes2 := []byte(str2)
	assert.False(t, isSimilarData(someBytes1, someBytes2))
}

func TestExitOnErrNil(t *testing.T) {
	exitOnErr("", nil)
}

func TestExitOnErrExit(t *testing.T) {
	if os.Getenv("EXIT_ON_ERR_EXIT") == "1" {
		exitOnErr("Some error", errors.New("Some error"))
	}

	cmd := exec.Command(os.Args[0], "-test.run=TestExitOnErrExit")
	cmd.Env = append(os.Environ(), "EXIT_ON_ERR_EXIT=1")
	err := cmd.Run()

	if e, ok := err.(*exec.ExitError); ok && !e.Success() {
		return
	}

	t.Fatalf("TestExitOnErrExit ran with error %v, expecting failed status", err)
}

func TestGetLockFilePreset(t *testing.T) {
	srcLockFile := "/var/lock/somefile.lock"
	lockFile := getLockFile(&srcLockFile)

	assert.NotNil(t, lockFile)
	assert.Equal(t, srcLockFile, *lockFile)
}

func TestGetLockFileNil(t *testing.T) {
	lockFile := getLockFile(nil)
	assert.NotNil(t, lockFile)
	assert.Equal(t, defaultLockFile, *lockFile)
}
