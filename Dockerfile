# compilation + test image
FROM varikin/golang-glide-alpine:latest as compiler

WORKDIR /go/src/gitlab.com/guangie88/hdfs-to-local
COPY glide.lock glide.yaml main.go ./
RUN glide install && \
    CGO_ENABLED=0 go build

# runtime image
FROM alpine:3.6
WORKDIR /app
COPY --from=compiler \
    /go/src/gitlab.com/guangie88/hdfs-to-local/hdfs-to-local \
    ./

ENTRYPOINT [ "/app/hdfs-to-local" ]